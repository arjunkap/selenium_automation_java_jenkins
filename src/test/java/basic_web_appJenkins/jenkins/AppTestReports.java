package basic_web_appJenkins.jenkins;

import static org.junit.Assert.fail;
import java.lang.reflect.Method;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
 
public class AppTestReports {
	
	static ExtentTest test;
	static ExtentReports report;
	
	@BeforeClass
	public static void startTest()
	{
	report = new ExtentReports(System.getProperty("user.dir")+"\\ExtentReport.html");
	
	}
 
    @Before
    public void beforeEachTestMethod() {
        System.out.println("Invoked before each test method");
        test = report.startTest("Test Case Name");    
       
    }
 
    @After
    public void afterEachTestMethod() {
        System.out.println("Invoked after each test method");
       
    }
 
    @AfterClass
    public static void endTest()
    {
    report.endTest(test);
    report.flush();
    }
 
    @Test
    public void testOne() {
    	
    	System.setProperty("webdriver.chrome.driver", "./chromedriveer.exe");
    	
    	WebDriver driver = new ChromeDriver();//step 1 web driver creation
    	driver.get("https://www.google.co.in");// step 2 web driver navigation
    	
    	if(driver.getTitle().equals("Google"))//step 3 locating an element
    	{
    	driver.findElement(By.name("q")).sendKeys("auspost");	//step 4,5 wait for return from the browser
    	test.log(LogStatus.PASS, "Navigated to the google URL");//
    	test.log(LogStatus.PASS, "Entered AusPost in the search bar");//step 6 recording / reporting
    	}
    	else
    	{
    	test.log(LogStatus.FAIL, "Test Failed");
    	}
    }
 
    @Test
    public void testTwo() {
    	System.setProperty("webdriver.chrome.driver", "./chromedriveer.exe");
    	WebDriver driver = new ChromeDriver();//step 1 web driver creation
    	driver.get("https://www.gmail.com");// step 2 web driver navigation

    	if(driver.getTitle().equals("Google"))//step 3 locating an element
    	{
    	driver.findElement(By.name("q")).sendKeys("auspost");	//step 4,5 wait for return from the browser
    	test.log(LogStatus.PASS, "Navigated to the specified URL");//
    	test.log(LogStatus.PASS, "Entered AusPost in the search bar");//step 6 recording / reporting
    	}
    	else
    	{	
    	fail("tes failed");
    	test.log(LogStatus.PASS, "Navigated to incorrect site");
    	test.log(LogStatus.FAIL, "Test Failed");
    	}
    }
   @Test
    public void testThree() {
    	
    	System.setProperty("webdriver.chrome.driver", "./chromedriveer.exe");
    	
    	WebDriver driver = new ChromeDriver();//step 1 web driver creation
    	driver.get("https://www.google.co.in");// step 2 web driver navigation
    	
    	if(driver.getTitle().equals("Google"))//step 3 locating an element
    	{
    	driver.findElement(By.name("q")).sendKeys("auspost");	//step 4,5 wait for return from the browser
    	test.log(LogStatus.PASS, "Navigated to the google URL");//
    	test.log(LogStatus.PASS, "Entered AusPost in the search bar");//step 6 recording / reporting
    	}
    	else
    	{
    	test.log(LogStatus.FAIL, "Test Failed");
    	}
    }
}
package basic_web_appJenkins.jenkins;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}
	@Before
    public void runBeforeTestMethod() {

		System.setProperty("webdriver.chrome.driver","./chromedriveer.exe");// initializing the chrome driver from root of this folder
    }
	
	/**
	 * Rigourous Test :-)
	 * 
	 * @throws URISyntaxException
	 */
	public void testAppAusPostPass() throws URISyntaxException {
		
		
		
		URI serverURL = null;

		serverURL = new URI("https://auspost.com.au/locate/");
		
		System.setProperty("webdriver.chrome.driver","./chromedriveer.exe");// initializing the chrome driver from root of this folder
	    ChromeOptions chromeOptions = new ChromeOptions();
	    
		chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		WebDriver driver = new ChromeDriver(chromeOptions);

		// Initialize the eyes SDK and set your private API key.
		driver.get(serverURL.toString());
		try {
			AusPostHome webElements = new AusPostHome(driver);
			
			// Click the "Click me!" button.
			driver.findElement(webElements.trackingInput).sendKeys("12345");
			driver.findElement(webElements.trackingButton).click();
			System.out.println("test");
		 
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Close the browser.
			driver.quit();

		}

	}

	public void testAppAusPostWait() throws URISyntaxException {
		
		
		
		URI serverURL = null;

		serverURL = new URI("https://auspost.com.au/locate/");
		System.setProperty("webdriver.chrome.driver","./chromedriveer.exe");// initializing the chrome driver from root of this folder

	    ChromeOptions chromeOptions = new ChromeOptions();
	    
		chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		WebDriver driver = new ChromeDriver(chromeOptions);

		// Initialize the eyes SDK and set your private API key.
		driver.get(serverURL.toString());
		try {
			AusPostHome webElements = new AusPostHome(driver);
			AusPostLocate webElementslocate = new AusPostLocate(driver);
			
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);//implicit wait
			driver.findElement(webElementslocate.postcode).sendKeys("2205");//input text
			
			driver.findElement(webElementslocate.shop).click();
			driver.get("https://auspost.com.au/shop/");
			driver.findElement(webElementslocate.shopSearch).sendKeys("supermart");
			driver.findElement(webElementslocate.search).click();
			
			  
			
			System.out.println("test");
		 
			

		} catch (Exception e) {
			fail("Test case failed");
			e.printStackTrace();
		} finally {
			// Close the browser.
			driver.quit();

		}

	}
	/**
	 * Rigourous Test :-)
	 * 
	 * @throws URISyntaxException
	 */
	public void testAppAusPostWaitExplicit() throws URISyntaxException {

		URI serverURL = null;

		serverURL = new URI("https://auspost.com.au");
		
		System.setProperty("webdriver.chrome.driver","./chromedriveer.exe");// initializing the chrome driver from root of this folder
	    ChromeOptions chromeOptions = new ChromeOptions();
	    
		chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		WebDriver driver = new ChromeDriver(chromeOptions);
		
		// Initialize the eyes SDK and set your private API key.
		driver.get(serverURL.toString());
		try {
			
			AusPostLocate webElementslocate = new AusPostLocate(driver);
			
			WebElement learnMore = webElementslocate.learnMore();//waiting for element to be loaded
			learnMore.click();	
			driver.get("https://auspost.com.au/locate");
			WebElement searchWait = webElementslocate.search();//waiting for element to be loaded
			searchWait.click(); 
			
			
			System.out.println("test");
		 
			

		} catch (Exception e) {
			fail("error");
			e.printStackTrace();
		} finally {
			// Close the browser.
			driver.quit();

		}

	}
	
	public void testAppLinkText() throws URISyntaxException {

		URI serverURL = null;

		serverURL = new URI("https://auspost.com.au/locate/");
		
		System.setProperty("webdriver.chrome.driver","./chromedriveer.exe");// initializing the chrome driver from root of this folder
	    ChromeOptions chromeOptions = new ChromeOptions();
	    
		chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		WebDriver driver = new ChromeDriver(chromeOptions);
		
		// Initialize the eyes SDK and set your private API key.
		driver.get(serverURL.toString());
		try {
			
			AusPostLocate webElementslocate = new AusPostLocate(driver);
			WebElement searchWait = webElementslocate.search();
			
			

		} catch (Exception e) {
			fail("error");
			e.printStackTrace();
		} finally {
			// Close the browser.
			driver.quit();

		}

	}



}

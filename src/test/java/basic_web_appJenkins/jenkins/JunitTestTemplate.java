package basic_web_appJenkins.jenkins;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
 
public class JunitTestTemplate {
 
    @BeforeClass
    public static void beforeAllTestMethods() {
        System.out.println("Invoked once before all test methods");
    }
 
    @Before
    public void beforeEachTestMethod() {
        System.out.println("Invoked test case one");
    }
 
    @After
    public void afterEachTestMethod() {
        System.out.println("Invoked after test case one");
    }
 
    @Test
    public void testOne() {
        System.out.println("Test One");
    }
 
    @Test
    public void testTwo() {
        System.out.println("Test Two");
    }
    @AfterClass
    public static void afterAllTestMethods() {
        System.out.println("Invoked once after all test methods");
    }
}
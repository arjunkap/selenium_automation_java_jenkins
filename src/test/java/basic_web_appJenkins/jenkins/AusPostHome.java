package basic_web_appJenkins.jenkins;

import java.net.URI;
import java.net.URISyntaxException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AusPostHome 
{	
	WebDriver driver;

	public AusPostHome(WebDriver driver)
	{
		this.driver =driver;
	}

	By trackingInput = By.cssSelector("#tt-form-input");
	By trackingButton = By.cssSelector("#trkb488628-658152");
	By businessButton = By.cssSelector("#ni523530-628236");
	By enterprise_GovButton = By.cssSelector("#secn-2-420486-573390");
	By personalButton = By.cssSelector("#secn-0-558432-498600");
	By olineCommunity = By.cssSelector("#skha-0-551784-393894");
	By aboutUs = By.cssSelector("#skha-1-498600-511896");
    By HelpSupport = By.cssSelector("#hs480318-706350");
}

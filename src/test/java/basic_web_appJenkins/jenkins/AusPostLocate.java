package basic_web_appJenkins.jenkins;

import java.net.URI;
import java.net.URISyntaxException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AusPostLocate 
{	
	WebDriver driver;
	
	public AusPostLocate(WebDriver driver)
	{
		this.driver =driver;
	}
	
	By search = By.name("PolSubmitFormBtn");
	By shop   = By.linkText("Shop");
	By postcode = By.cssSelector("#PolSearchLocationInput");
	By learnMore = By.id("516882-455388");
	//WebElement searchWait=wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PolSubmitFormBtn")));
	
	//shop page
	By shopSearch = By.name("SearchTerm");
	
	public WebElement search()
	{
		WebElement searchWait = (new WebDriverWait(driver, 10))
				  .until(ExpectedConditions.presenceOfElementLocated(By.name("PolSubmitFormBtn")));
		return searchWait;
	}
	public WebElement learnMore()
	{
		WebElement searchWait = (new WebDriverWait(driver, 10))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("516882-455388")));
		return searchWait;
	}
	
}
